const { createConnection, getConnection, getRepository } = require("typeorm");

const chai = require("chai");
const chaiHttp = require("chai-http");

chai.use(chaiHttp);
const {
	EmployeeEntity,
	CheckingInEntity,
	CheckingOutEntity,
} = require("../build/src/db/entities");
const { EndOfDayEntity } = require("../build/src/db/entities/EndOfDay");

const should = chai.should();
const serverHost = "http://localhost:4000";

beforeEach(() => {
	return createConnection({
		type: "postgres",
		host: "localhost",
		port: 5433,
		username: "postgres",
		password: "randomPassword@3",
		database: "checkingApiTest",
		entities: [
			EmployeeEntity,
			CheckingInEntity,
			CheckingOutEntity,
			EndOfDayEntity,
		],
		synchronize: true,
	});
});

describe("Employee", () => {
	beforeEach(async (done) => {
		await getRepository(EmployeeEntity)
			.clear()
			.then(() => done());
	});
});

describe("/Get Employee", () => {
	it("it should get  all employees", (done) => {
		chai
			.request(serverHost)
			.get("/employee")
			.end((err, res) => {
				res.should.have.status(200);
				res.body.should.be.a("array");
				res.body.length.should.be.eql(0);
				done();
			});
	});
});

describe("/Post employee", () => {
	it("/Post  it should not create employee name required", (done) => {
		const employee = {
			firstName: "John",
			department: "IT",
		};
		chai
			.request(serverHost)
			.post("/employee")
			.send(employee)
			.end((err, res) => {
				res.should.have.status(422);
				res.body.should.be.a("object");
                res.body.should.have.property("message").eql("Validation failed");
				done();
			});
	});
});

afterEach(() => {
	const connection = getConnection();
	return connection.close();
});
