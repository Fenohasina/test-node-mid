import { Employee } from "../models/Employee";
import {
	Route,
	Controller,
	Post,
	Body,
	Response,
	SuccessResponse,
	Get,
	Path,
	Example,
} from "tsoa";
import {
	EmployeesCreationParams,
	EmployeesService,
} from "../services/Employee";
import moment from "moment";

interface ValidateErrorJSON {
	message: "Validation failed";
	details: { [name: string]: unknown };
}

@Route("employee")
export class EmployeesController extends Controller {
	/**
	 * Create new employee
	 */

	@Response<ValidateErrorJSON>(422, "Validation failed")
	@SuccessResponse("201", "Created")
	@Post()
	public async createEmployee(
		@Body() requestBody: EmployeesCreationParams
	): Promise<Employee> {
		this.setStatus(201);
		return new EmployeesService().create(requestBody);
	}
	/**
	 * Get list of employees
	 * @param dateCreated is date of creation
	 */
	@Get("{dateCreated}")
	@Example<Employee>({
		id: "929a262d-230c-4903-a93a-a43546ed13c0",
		dateCreated: moment().toDate(),
		department: "IT",
		firstName: "John",
		name: "Doe",
	})
	public async getEmployeeByDate(
		@Path() dateCreated: Date
	): Promise<Employee[]> {
		return new EmployeesService().getByDate(dateCreated);
	}

	/**
	 * Get list of employees
	 * @param dateCreated is date of creation
	 */
	@Get()
	public async getAllEmployee(): Promise<Employee[]> {
		return new EmployeesService().getAll();
	}
}
