import {
	Route,
	Post,
	Body,
	SuccessResponse,
	Controller
} from "tsoa";
import { Checking } from "../models/Checking";
import { CheckingService, MakeCheckingParams } from "../services/Checking";


@Route("check-in")
export class CheckingInController  extends Controller {
	/**
	 * Make check-in
	 * @param employeeID id of the employee
	 * @param comment  comment
	 */
	@SuccessResponse("201", "Created")
	@Post()
	public async CheckingIn(@Body() makeCheckingParams: MakeCheckingParams): Promise<Checking> {
         return new CheckingService().checkingIn(makeCheckingParams)
	}
}

@Route('check-out')
export class CheckingOutController extends Controller {
	/**
	 * Make check-out
	 * @param employeeID id of the employee
	 * @param comment  comment
	 */
	@SuccessResponse("201", "Created")
	@Post()
	public async CheckingOut(@Body() makeCheckingParams: MakeCheckingParams): Promise<Checking> {
		return new CheckingService().checkingOut(makeCheckingParams)
	}
}