export interface Checking {
    employeeId: string,
    hour: Date,
    comment: string,
}