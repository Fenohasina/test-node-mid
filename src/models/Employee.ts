export interface Employee {
    id:string,
    name: string,
    firstName: string,
    department: string
    dateCreated: Date,
}