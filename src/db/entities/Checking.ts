import {
	Entity,
	PrimaryGeneratedColumn,
	CreateDateColumn,
	Column,
} from "typeorm";

@Entity({ name: "checking_in" })
export class CheckingInEntity {
	@PrimaryGeneratedColumn("uuid")
	id!: string;

	@CreateDateColumn({ type: "timestamp" })
	hour!: Date;

	@Column({type: 'text'})
	employeeId!: string;
}



@Entity({ name: "checking_out" })
export class CheckingOutEntity {
    @PrimaryGeneratedColumn("uuid")
    id!: string;

    @CreateDateColumn({ type: "timestamp" })
    hour!: Date;

    @Column({ type: 'text' })
    employeeId!: string;
}