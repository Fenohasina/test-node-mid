
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("end-of-day")
export class EndOfDayEntity {
    @PrimaryGeneratedColumn("uuid")
	employeeId!: string;

	@Column({type: "timestamp without time zone"})
	duration!: Date;
}
