import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn } from "typeorm";
@Entity("employee")
export class EmployeeEntity {
	@PrimaryGeneratedColumn("uuid")
    id!: string;

	@Column()
	name!: string;

	@Column()
	firstName!: string;

	@Column()
	department!: string;

	@CreateDateColumn({type: 'date'})
	dateCreated!: Date;
}
