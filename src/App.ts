import express, {
	Response as AppResponse,
	Request as AppRequest,
	NextFunction,
} from "express";
import bodyParser from "body-parser";
import { RegisterRoutes } from "../build/routes";
import { ValidateError } from "tsoa";
import { createConnection } from "typeorm";
import * as swaggerUi from 'swagger-ui-express'


const swaggerDocument = require('../swagger.json');
export const app = express();

app.use(
	bodyParser.urlencoded({
		extended: true,
	})
);

app.use(bodyParser.json());
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
RegisterRoutes(app);
// Handle route errors
app.use(function notFoundHandler(_req, res: AppResponse) {
	res.status(404).send({
		message: "Not found",
	});
});
app.use(function errorHandler(
	err: unknown,
	req: AppRequest,
	res: AppResponse,
	next: NextFunction
): AppResponse | void {
	// Handle validation errors
	if (err instanceof ValidateError) {
		console.warn(`Caught validation error for ${req.path}`, err.fields);
		return res.status(422).json({
			message: "Validation failed",
			details: err?.fields,
		});
	}
	if (err instanceof Error) {
		return res.status(500).json({
			message: `Internal server error ${err}`,
		});
	}
	next();
});

createConnection()
    .then(() => console.log("App connected to the database"))
	.catch((e) => console.error({ message: e }));
