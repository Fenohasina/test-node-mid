import { EntityRepository, getConnection, Repository } from "typeorm";
import { EmployeeEntity } from "../db/entities";
import { Employee } from "../models/Employee";

export type EmployeesCreationParams = Pick<
	Employee,
	"name" | "firstName" | "department"
>;

@EntityRepository(EmployeeEntity)
class EmployeeRepository extends Repository<EmployeeEntity> {}
export class EmployeesService {
	private employeeRepository: EmployeeRepository;

	constructor() {
		this.employeeRepository =
			getConnection().getCustomRepository(EmployeeRepository);
	}

	public async create(employeesCreationParams: EmployeesCreationParams): Promise<Employee> {
		return this.employeeRepository.save(employeesCreationParams);
	}

    public async getByDate(dateCreated: Date):  Promise<Employee[]> {
		return this.employeeRepository.find({where:{dateCreated}})
	}

    public async getAll(): Promise<Employee[]> {
        return this.employeeRepository.find()
    }
}
