import moment from "moment";
import { EntityRepository, getConnection, Repository, Between } from "typeorm";
import { CheckingInEntity, CheckingOutEntity } from "../db/entities";
import { EndOfDayEntity } from "../db/entities/EndOfDay";
import { Checking } from "../models/Checking";

export type MakeCheckingParams = Pick<Checking, "employeeId" | "comment">;

@EntityRepository(CheckingInEntity)
export class CheckingInRepository extends Repository<CheckingInEntity> {}

@EntityRepository(CheckingOutEntity)
export class CheckingOutRepository extends Repository<CheckingOutEntity> {}

@EntityRepository(EndOfDayEntity)
export class EndOfDayRepository extends Repository<EndOfDayEntity> {}

export class CheckingService {
	private checkingInRepository: CheckingInRepository;
	private checkingOutRepository: CheckingOutRepository;
	private endOfDayRepository: EndOfDayRepository;

	constructor() {
		this.checkingInRepository =
			getConnection().getCustomRepository(CheckingInRepository);
		this.checkingOutRepository = getConnection().getCustomRepository(
			CheckingOutRepository
		);
		this.endOfDayRepository =
			getConnection().getCustomRepository(EndOfDayRepository);
	}
	public async checkingIn(
		makeCheckingParams: MakeCheckingParams
	): Promise<Checking> {
		return this.checkingInRepository.save(makeCheckingParams);
	}

	public async checkingOut(
		makeCheckingParams: MakeCheckingParams
	): Promise<Checking> {
		const checkout = await this.checkingOutRepository.save(makeCheckingParams);
		const checkingIn = await this.checkingInRepository.findOne({
			where: {
				employeeId: makeCheckingParams.employeeId,
				hour: Between(
					`${moment(new Date(), "YYYY-MM-DD").startOf("D").toISOString()}`,
					`${moment(new Date(), "YYYY-MM-DD").endOf("D").toISOString()}`
				),
			},
		});

      
		await this.endOfDayRepository.save({
			duration: moment(checkingIn?.hour).diff(new Date(), "milliseconds"),
			employeeId: makeCheckingParams.employeeId,
		});
		return checkout;
	}
}
