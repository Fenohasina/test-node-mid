FROM  node:16.13.0

WORKDIR /server

# Copy package.json for caching node_modules
COPY package.json /server/package.json

# Copy yarn.lock for packages consistency
COPY yarn.lock /server

# Install packages
RUN yarn install

COPY src /server/src

COPY tsconfig.json /server

COPY ormconfig.js /server

COPY tsoa.json /server

RUN yarn build

EXPOSE 4000

ENV POSTGRES_HOST postgres
ENV POSTGRES_PORT 5432


COPY . .

CMD ./wait-for-it.sh -t 60 -h $POSTGRES_HOST -p $POSTGRES_PORT -- yes | npm start
