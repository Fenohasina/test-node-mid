module.exports = {
	type: "postgres",
	host: "db",
	port: 5432,
	username: "postgres",
	password: "randomPassword@3",
	database: "checkingApi",
	entities: ["./build/src/db/entities/**/*.js"],
	migrations: ["migrations/*.js"],
	cli: {
		migrationsDir: "migrations",
	},
};
