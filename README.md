# Attendance API

The main usage of this app is for attendance

## Stack
- nodeJs
- Typeorm
- [Tsoa](https://tsoa-community.github.io/docs/)
- Mocha and chai for the unit test

Tsoa help get accurate, complete, correct and up-to-date OpenAPI Specifications from your TypeScript code. And it's typesafe
## Installation

The app is Dockerized:
 - First Install npm packages using yarn 

```bash
yarn install
```
- And then run the docker-compose
```bash
docker-compose up -d
```

This will pull all images required: Postgres, Pgadmin 4 and will build the node js server image

Once its running it is reachable at http://localhost:4000
and the swagger documentation at http://localhost:4000/api-docs

##### note:
If i had enough time for this: 
 - I would have create a pagination abstraction to the list
 - I would have covered all features with test